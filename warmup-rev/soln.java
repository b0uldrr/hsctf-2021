import java.util.Scanner;

public class soln {
  
	public static int[] anticold(int [] arr) {
		int[] ret = new int[33];

		int index = 0;
		for (int i = 16; i < 33; i++) {
			ret[index] = arr[i];	
			index++;
		}

		for (int i = 0; i < 16; i++) {
			ret[index] = arr[i];	
			index++;
		}
		return ret;
	}
	
	public static int[] anticool(int [] arr) {
		int[] ret = new int[33];
		for (int i = 0; i < 33; i++)
			if (i % 2 == 0) {
				ret[i] =  arr[i] - 3 * (i / 2);
			}
			else {
				ret[i] = arr[i];
			}
		return ret;
	}

	public static int[] antiwarm(int [] arr, int val) {
		int[] ret = new int[33];

		for(int i=31; i>0; i--) {
			if(arr[i] == 108) {
				int index = 0;
				for(int j=i+1; j<33; j++) {
					ret[index] = arr[j];	
					index++;
				}
				for(int j=i-val; j<=i; j++) {
					ret[index] = arr[j];	
					index++;
				}
				for(int j=0; j<i-val; j++) {
					ret[index] = arr[j];	
					index++;
				}
				break;
			}
		}

		return ret;
	}

	public static int[] antihot(int [] arr) {
		int[] ret = new int[33];

		int[] adj = {-72, 7, -58, 2, -33, 1, -102, 65, 13, -64, 
				21, 14, -45, -11, -48, -7, -1, 3, 47, -65, 3, -18, 
				-73, 40, -27, -73, -13, 0, 0, -68, 10, 45, 13};

		for (int i = 0; i < 33; i++) {
			ret[i] = arr[i] - adj[i];
		}

		return ret;
	}

	public static String reverseString(String t) {
		// getBytes() method to convert string into bytes[].
		byte[] strAsByteArray = t.getBytes();
		byte[] result = new byte[strAsByteArray.length];

		// Store result in reverse order into the result byte[]
		for (int i = 0; i < strAsByteArray.length; i++)
		    result[i] = strAsByteArray[strAsByteArray.length - i - 1];

		return new String(result);
	}
	

	public static int[] stringToArr(String t) {
		int[] ret = new int[33];
		for (int i = 0; i < 33; i++)
			ret[i] = t.charAt(i);
		return ret;
	}

	public static String arrToString(int [] arr) {
		String ret = "";
		for (int i = 0; i < 33; i++)
			ret += (char)arr[i];
		return ret;
	}
		

	public static void main(String[] args) {
		String match = "4n_3nd0th3rm1c_rxn_4b50rb5_3n3rgy";

		int[] ret = new int[33];

		ret = stringToArr(match);
		ret = antihot(ret);
	
		for(int i=0; i<27; i++) {
			System.out.println(arrToString(anticold(anticool(antiwarm(ret, i)))));
		}
	}
  
}
