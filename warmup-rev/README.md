## warmup-rev

* **CTF:** HSCTF 2021
* **Category:** rev
* **Points:** 371
* **Author(s):** b0uldrr
* **Date:** 20/06/21

---

### Challenge
```
Time to get warmed up!
```

### Downloads
* [WarmupRev.java](WarmupRev.java)

---

### Solution

The provided source code, `WarmupRev.java`, prompts for the user the enter the flag. It then runs that input through a series of test functions - `cold()`, `cool()`, `warm()`, and `hot()` which each manipulates the input in various ways before comparing the resultant outcome against the encrypted flag, held in the `hot()` function.

WarmupRev.java:
```java
import java.util.Scanner;

public class WarmupRev {
  
        public static String cold(String t) {
                return t.substring(17) + t.substring(0, 17);
        }

        public static String cool(String t) {
                String s = "";
                for (int i = 0; i < t.length(); i++)
                        if (i % 2 == 0)
                                s += (char) (t.charAt(i) + 3 * (i / 2));
                        else
                                s += t.charAt(i);
                return s;
        }

        public static String warm(String t) {
                String a = t.substring(0, t.indexOf("l") + 1);
                String t1 = t.substring(t.indexOf("l") + 1);
                String b = t1.substring(0, t1.indexOf("l") + 1);
                String c = t1.substring(t1.indexOf("l") + 1);
                return c + b + a;
        }

        public static String hot(String t) {
                int[] adj = {-72, 7, -58, 2, -33, 1, -102, 65, 13, -64, 
                                21, 14, -45, -11, -48, -7, -1, 3, 47, -65, 3, -18, 
                                -73, 40, -27, -73, -13, 0, 0, -68, 10, 45, 13};
                String s = "";
                for (int i = 0; i < t.length(); i++)
                        s += (char) (t.charAt(i) + adj[i]);
                return s;
        }

        public static void main(String[] args) {
                Scanner in = new Scanner(System.in);
                System.out.print("Let's get warmed up! Please enter the flag: ");
                String flag = in.nextLine();
                String match = "4n_3nd0th3rm1c_rxn_4b50rb5_3n3rgy";
                if (flag.length() == 33 && hot(warm(cool(cold(flag)))).equals(match))
                        System.out.println("You got it!");
                else
                        System.out.println("That's not correct, please try again!");
                in.close();
        }
  
}
```

My job was to reverse each of those function to retrieve the plaintext flag.

Reversing `cool()`, `cold()` and `hot()` was fairly straight forward. `warm()` however, was harder because it is not 1:1 reverseable. Once a string has been run through `warm()`, there is no way to reverse the output back to the original string. At best, we are left with 27 different possible outcomes, only one of them being the correct original string. My solution was just to try all of them in a loop and then look for the correct flag in the output.

soln.java
```java
import java.util.Scanner;

public class soln {
  
        public static int[] anticold(int [] arr) {
                int[] ret = new int[33];

                int index = 0;
                for (int i = 16; i < 33; i++) {
                        ret[index] = arr[i];
                        index++;
                }

                for (int i = 0; i < 16; i++) {
                        ret[index] = arr[i];
                        index++;
                }
                return ret;
        }

        public static int[] anticool(int [] arr) {
                int[] ret = new int[33];
                for (int i = 0; i < 33; i++)
                        if (i % 2 == 0) {
                                ret[i] =  arr[i] - 3 * (i / 2);
                        }
                        else {
                                ret[i] = arr[i];
                        }
                return ret;
        }

        public static int[] antiwarm(int [] arr, int val) {
                int[] ret = new int[33];

                for(int i=31; i>0; i--) {
                        if(arr[i] == 108) {
                                int index = 0;
                                for(int j=i+1; j<33; j++) {
                                        ret[index] = arr[j];
                                        index++;
                                }
                                for(int j=i-val; j<=i; j++) {
                                        ret[index] = arr[j];
                                        index++;
                                }
                                for(int j=0; j<i-val; j++) {
                                        ret[index] = arr[j];
                                        index++;
                                }
                                break;
                        }
                }

                return ret;
        }

        public static int[] antihot(int [] arr) {
                int[] ret = new int[33];

                int[] adj = {-72, 7, -58, 2, -33, 1, -102, 65, 13, -64, 
                                21, 14, -45, -11, -48, -7, -1, 3, 47, -65, 3, -18, 
                                -73, 40, -27, -73, -13, 0, 0, -68, 10, 45, 13};

                for (int i = 0; i < 33; i++) {
                        ret[i] = arr[i] - adj[i];
                }

                return ret;
        }

        public static String reverseString(String t) {
                // getBytes() method to convert string into bytes[].
                byte[] strAsByteArray = t.getBytes();
                byte[] result = new byte[strAsByteArray.length];

                // Store result in reverse order into the result byte[]
                for (int i = 0; i < strAsByteArray.length; i++)
                    result[i] = strAsByteArray[strAsByteArray.length - i - 1];

                return new String(result);
        }


        public static int[] stringToArr(String t) {
                int[] ret = new int[33];
                for (int i = 0; i < 33; i++)
                        ret[i] = t.charAt(i);
                return ret;
        }

        public static String arrToString(int [] arr) {
                String ret = "";
                for (int i = 0; i < 33; i++)
                        ret += (char)arr[i];
                return ret;
        }


        public static void main(String[] args) {
                String match = "4n_3nd0th3rm1c_rxn_4b50rb5_3n3rgy";

                int[] ret = new int[33];

                ret = stringToArr(match);
                ret = antihot(ret);

                for(int i=0; i<27; i++) {
                        System.out.println(arrToString(anticold(anticool(antiwarm(ret, i)))));
                }
        }
  
}
```

Running it, we can see the flag about half way down the output:
```
$ java soln
Picked up _JAVA_OPTIONS: -Dawt.useSystemAAFontSettings=on -Dswing.aatext=true
[]D^PXyG0N_y}N3nth4lc|["Q[
CsB_@nnyUk      u5GLJM3nth4lulpg1}c3
]A^MUyD0K_▒y▒3nth4lt~`|XN
~3@s?_=nkyRku2GI3nth4lA}rlmg1zc
K▒[U]>^JRyA0H_3nth4lpJq~]|U
wc{3=s<_:nhyOku/3nth4l>y>}oljg1
H[R];^GOy>0E3nth4lVGmJn~Z|R
1tcx3:s9_7neyLk3nth4ll_;y;}llgg
OE[O]8^DLy;3nth4l'uSGjJk~W|
dg~1qcu37s6_4nbyI3nth4lb0i_8y8}il
T|LB[L]5^AI3nth4lpk$uPGgJh~
flag{1ncr34s3_1n_3nth4lpy_0f_5y5}
e~Q|I?
      [I]2^>3nth4lymk!uMGdJ
2}cl^gx1kco31s0_.3nth4lemy\0c_2y
<Jb~N|F [F]/3nth4lUnyjkuJG
/y/}`l[gu1hcl3.s-3nth4lV^bjyY0`_
GG^J_~K|C
9[C3nth4lT_Rnygk
]_,y,}]lXgr1eci3+3nth4lj]S^_gyV0
▒uDG[J\~H|@63nth4lRsQ_On}ydk
S0Z_)y)}ZlUgo1bcf3nth4l*[g]P^\dy
akuAGXJY~E|=33nth4l3OsN_Lnzy
ayP0W_&y&}WlRgl1_3nth4lZ'[d]M^Y
wy^ku>GUJV~B|:3nth4lc3LsK_In
V^yM0T_#y#}TlOgi3nth4l(W$[a]J^
Fnty[ku;GRJS~?|73nth4l1c3IsH_
G^S[yJ0Q_ y }QlL3nth4l^%T![^]
E_CnqyXk
        u8GOJP~<3nth4lsg1c3Fs
```

---

### Flag 
```
flag{1ncr34s3_1n_3nth4lpy_0f_5y5}
```
