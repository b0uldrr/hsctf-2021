## not-really-math

* **CTF:** HSCTF 2021
* **Category:** misc
* **Points:** 319  
* **Author(s):** b0uldrr
* **Date:** 20/06/21 

---

### Challenge
```
Have a warmup algo challenge.

nc not-really-math.hsc.tf 1337
```

### Downloads
* 

---

### Solution

Connecting to the listening port, we are presented with a series of mathematical equations to solve. With `m` represnted multiplication and `a` addition. In this challange, addition takes order precedence over multiplication.

```
Sample Input:

2m3a19m2a38m1

Sample Output:

1760

Explanation:

2*(3+19)*(2+38)*1 = 1760
```

My solution:
```py
#!/usr/bin/python3
import pwn
import re

conn = pwn.remote('not-really-math.hsc.tf', 1337)


def processQuestion(q):
        print(q)

        numbers = re.split("m|a", q)

        operators = []
        for i in q:
                if ((i == 'a') or (i == 'm')):
                        operators.append(i)

        i = 0
        while(i < len(operators)):
                if(operators[i] == 'a'):
                        numbers[i] = int(numbers[i]) + int(numbers[i+1])
                        numbers.pop(i+1)
                        operators.pop(i)
                else:
                        i = i + 1

        answer = 1
        for i in numbers:
                answer = (answer * int(i)) % (2**32 - 1) 

        return str(answer)

    
conn.readline()

while(True):
        answer = processQuestion(conn.readline().decode().strip())
        print(answer)
        conn.sendlineafter(": ", answer)
```

---

### Flag 
```
flag{yknow_wh4t_3ls3_is_n0t_real1y_math?_c00l_m4th_games.com}
```
