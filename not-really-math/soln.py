#!/usr/bin/python3
import pwn
import re

conn = pwn.remote('not-really-math.hsc.tf', 1337)


def processQuestion(q):
	print(q)

	numbers = re.split("m|a", q)

	operators = []
	for i in q:
		if ((i == 'a') or (i == 'm')):
			operators.append(i)

	i = 0
	while(i < len(operators)):
		if(operators[i] == 'a'):
			numbers[i] = int(numbers[i]) + int(numbers[i+1])
			numbers.pop(i+1)
			operators.pop(i)
		else:
			i = i + 1

	answer = 1
	for i in numbers:
		answer = (answer * int(i)) % (2**32 - 1) 

	return str(answer)

    
conn.readline()

while(True):
	answer = processQuestion(conn.readline().decode().strip())
	print(answer)
	conn.sendlineafter(": ", answer)

