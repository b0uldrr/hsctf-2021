#!/usr/bin/python3
# uncompyle6 version 3.7.4
# Python bytecode 3.8 (3413)
# Decompiled from: Python 2.7.17 (default, Sep 30 2020, 13:38:04) 
# [GCC 7.5.0]
# Warning: this version of Python has problems handling the Python 3 "byte" type in constants properly.

# Embedded file name: sneks.py
# Compiled at: 2021-05-19 21:21:59
# Size of source mod 2**32: 600 bytes
import sys
import string

def f(n):
    if n == 0:
        return 0
    if n == 1 or n == 2:
        return 1
    x = f(n >> 1)
    y = f(n // 2 + 1)
    return g(x, y, not n & 1)


def e(b, j):
    return 5 * f(b) - 7 ** j


def d(v):
    return v << 1


def g(x, y, l):
    if l:
        return h(x, y)
    return x ** 2 + y ** 2


def h(x, y):
    return x * j(x, y)


def j(x, y):
    return 2 * y - x


def do_stuff(flag):
    ret = []
    inp = bytes(flag, 'utf-8')
    a = []
    for i, c in enumerate(inp):
        a.append(e(c, i))
    else:
        for c in a:
            ret.append((d(c)))

    return ret

def solve():
	f = open("output.txt", "r")
	codes = f.readline().strip().split()
	codes = [int(code) for code in codes]

	found_count = 5
	flag = "flag{"
	while(flag[-1] != "}"):
		for c in string.printable:
			if(do_stuff(flag + c) == codes[0:found_count+1]):
				flag = flag + c
				found_count += 1
				break

	print(flag)

if __name__ == '__main__':
	solve()
