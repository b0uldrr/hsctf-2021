## sneks

* **CTF:** HSCTF 2021 
* **Category:** rev
* **Points:** 403
* **Author(s):** b0uldrr
* **Date:** 20/06/21

---

### Challenge
```
https://www.youtube.com/watch?v=0arsPXEaIUY
```

### Downloads
* [sneks.pyc](sneks.pyc)
* [output.txt](output.txt)

---

### Solution
We're provided with a compiled python binary, `sneks.pyc`. I decompiled it at https://www.decompiler.com/. I would normally use `decompyle6` but it was having issues on my machine and this online service was a quick work around.

we're also provided with `output.txt`, which contains a series of long numbers separated by spaces.

By reading the decompiled source code of `sneks.pyc`, we can see that it takes a string from the command line and then runs it through a series of functions to encrypt it before printing the encrypted output to stdout. `output.txt` contains the encrypted flag, and we need to reverse the program to retrieve it.

I chose to brute force the answer. We can see that by running `./sneks.pyc flag{` we get the first 5 correct values in `output.txt`, so my solution was to keep create a function which looped through every printable character and append that onto the end of the current "known" flag until the next output value was correct, then repeat until the entire flag was found.

```
$ ./sneks.pyc flag{
9273726921930789991758 166410277506205636620946 836211434898484229672 15005205362068960832084 226983740520068639569752018 
```

Decompiled source of `sneks.pyc`:
```py
#!/usr/bin/python3
# uncompyle6 version 3.7.4
# Python bytecode 3.8 (3413)
# Decompiled from: Python 2.7.17 (default, Sep 30 2020, 13:38:04) 
# [GCC 7.5.0]
# Warning: this version of Python has problems handling the Python 3 "byte" type in constants properly.

# Embedded file name: sneks.py
# Compiled at: 2021-05-19 21:21:59
# Size of source mod 2**32: 600 bytes
import sys

def f(n):
    if n == 0:
        return 0
    if n == 1 or n == 2:
        return 1
    x = f(n >> 1)
    y = f(n // 2 + 1)
    return g(x, y, not n & 1)


def e(b, j):
    return 5 * f(b) - 7 ** j


def d(v):
    return v << 1


def g(x, y, l):
    if l:
        return h(x, y)
    return x ** 2 + y ** 2


def h(x, y):
    return x * j(x, y)


def j(x, y):
    return 2 * y - x


def main():
    if len(sys.argv) != 2:
        print('Error!')
        sys.exit(1)
    inp = bytes(sys.argv[1], 'utf-8')
    a = []
    for i, c in enumerate(inp):
        a.append(e(c, i))
    else:
        for c in a:
            print((d(c)), end=' ')


if __name__ == '__main__':
    main()
```

output.txt:
```
9273726921930789991758 166410277506205636620946 836211434898484229672 15005205362068960832084 226983740520068639569752018 4831629526120101632815236 203649875442 1845518257930330962016244 12649370320429973923353618 203569403526 435667762588547882430552 2189229958341597036774 175967536338 339384890916 319404344993454853352 -9165610218896 435667762522082586241848 3542248016531591176336 319401089522705178152 -22797257207834556 12649370160845441339659218 269256367990614644192076 -7819641564003064368 594251092837631751966918564
```

My solution:
```py
#!/usr/bin/python3

import sys
import string

def f(n):
    if n == 0:
        return 0
    if n == 1 or n == 2:
        return 1
    x = f(n >> 1)
    y = f(n // 2 + 1)
    return g(x, y, not n & 1)


def e(b, j):
    return 5 * f(b) - 7 ** j


def d(v):
    return v << 1


def g(x, y, l):
    if l:
        return h(x, y)
    return x ** 2 + y ** 2


def h(x, y):
    return x * j(x, y)


def j(x, y):
    return 2 * y - x


def do_stuff(flag):
    ret = []
    inp = bytes(flag, 'utf-8')
    a = []
    for i, c in enumerate(inp):
        a.append(e(c, i))
    else:
        for c in a:
            ret.append((d(c)))

    return ret

def solve():
        f = open("output.txt", "r")
        codes = f.readline().strip().split()
        codes = [int(code) for code in codes]

        found_count = 5
        flag = "flag{"
        while(flag[-1] != "}"):
                for c in string.printable:
                        if(do_stuff(flag + c) == codes[0:found_count+1]):
                                flag = flag + c
                                found_count += 1
                                break

        print(flag)

if __name__ == '__main__':
        solve()
```

Running it, we retrieve the flag.
```
$ ./soln.py
flag{s3qu3nc35_4nd_5um5}
```

---

### Flag 
```
flag{s3qu3nc35_4nd_5um5}
```
