## seeded-randomizer

* **CTF:** HSCTF 2021 
* **Category:** rev
* **Points:** 381 
* **Author(s):** b0uldrr
* **Date:** 20/06/21

---

### Challenge
```
There are at least two types of randomizers in Java, one that is purely random and one that is seeded (that is pseudorandom). Please fix this code to output the correct flag (note the flag format, and a sample has been provided).
```

### Downloads
* [SeededRandomizer.java](SeededRandomizer.java)

---

### Solution

See the provided source code for `SeededRandomizer.java` below. The flag is stored encrypted in the `c` array. To decrypt it, we just need to know the value that was used to seed the `Random()` function when the flag was intially encrypted. Importantly, there is a comment in the source that the seed is between 0 and 1000:  
```
Instantiate another seeded randomizer below (seed is integer between 0 and 1000, exclusive):
```

SeededRandomizer.java:
```java
import java.util.Random;

public class SeededRandomizer {

        public static void display(char[] arr) {
                for (char x: arr)
                        System.out.print(x);
                System.out.println();
        }

        public static void sample() {
                Random rand = new Random(79808677);
                char[] test = new char[12];
                int[] b = {9, 3, 4, -1, 62, 26, -37, 75, 83, 11, 30, 3};
                for (int i = 0; i < test.length; i++) {
                        int n = rand.nextInt(128) + b[i];
                        test[i] = (char)n;
                }
                display(test);
        }

        public static void main(String[] args) {
                // sample();
                // Instantiate another seeded randomizer below (seed is integer between 0 and 1000, exclusive):
                char[] flag = new char[33];
                int[] c = {13, 35, 15, -18, 88, 68, -72, -51, 73, -10, 63, 
                                1, 35, -47, 6, -18, 10, 20, -31, 100, -48, 33, -12, 
                                13, -24, 11, 20, -16, -10, -76, -63, -18, 118};
                for (int i = 0; i < flag.length; i++) {
                        int n = (int)(Math.random() * 128) + c[i];
                        flag[i] = (char)n;
                }
                display(flag);

        }

}
```

My solution was to just try every seed value from 0 to 1000, checking if the first few characters read "flag":
```java
import java.util.Random;

public class soln {

        public static void display(char[] arr) {
                for (char x: arr)
                        System.out.print(x);
                System.out.println();
        }

        public static void main(String[] args) {
                // Instantiate another seeded randomizer below (seed is integer between 0 and 1000, exclusive):
                for(int seed=0; seed<1000; seed++) {
                        Random rand = new Random(seed);
                        char[] flag = new char[33];
                        int[] c = {13, 35, 15, -18, 88, 68, -72, -51, 73, -10, 63, 
                                        1, 35, -47, 6, -18, 10, 20, -31, 100, -48, 33, -12, 
                                        13, -24, 11, 20, -16, -10, -76, -63, -18, 118};
                        for (int i = 0; i < flag.length; i++) {
                                int n = rand.nextInt(128) + c[i];
                                flag[i] = (char)n;
                        }
                        if(flag[0] == 'f' && flag[1] == 'l' && flag[2] == 'a' && flag[3] == 'g') {
                                display(flag);
                        }
                }

        }

}
```

Running it, we get the flag:
```
$ javac soln.java
$ java soln
Picked up _JAVA_OPTIONS: -Dawt.useSystemAAFontSettings=on -Dswing.aatext=true
flag{s33d3d_r4nd0m1z3rs_4r3_c00l}
```

---

### Flag 
```
flag{s33d3d_r4nd0m1z3rs_4r3_c00l}
```
