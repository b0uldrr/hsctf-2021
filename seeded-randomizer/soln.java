import java.util.Random;

public class soln {

	public static void display(char[] arr) {
		for (char x: arr)
			System.out.print(x);
		System.out.println();
	}

	public static void main(String[] args) {
		// Instantiate another seeded randomizer below (seed is integer between 0 and 1000, exclusive):
		for(int seed=0; seed<1000; seed++) {
			Random rand = new Random(seed);
			char[] flag = new char[33];
			int[] c = {13, 35, 15, -18, 88, 68, -72, -51, 73, -10, 63, 
					1, 35, -47, 6, -18, 10, 20, -31, 100, -48, 33, -12, 
					13, -24, 11, 20, -16, -10, -76, -63, -18, 118};
			for (int i = 0; i < flag.length; i++) {
				int n = rand.nextInt(128) + c[i];
				flag[i] = (char)n;
			}
			if(flag[0] == 'f' && flag[1] == 'l' && flag[2] == 'a' && flag[3] == 'g') {
				display(flag);
			}
		}
	
	}

}
