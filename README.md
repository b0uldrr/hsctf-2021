## HSCTF 2021

* **CTF Time page:** https://ctftime.org/event/1264 
* **Category:** Jeopardy
* **Date:** Mon, 14 June 2021, 12:00 UTC — Sat, 19 June 2021, 00:00 UTC

---

### Solved Challenges
| Name | Category | Points | Key Concepts |
|------|----------|--------|--------------|
|not-really-math|misc|319|python|
|seeded-randomizer|rev|381|java, random|
|sneks|rev|403|python, compiled, printable|
|stonks|pwn|391|bof, 64-bit, Ghidra|
|warmup-rev|rev|371|java|
